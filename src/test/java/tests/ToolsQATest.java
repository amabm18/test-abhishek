package tests;

import common.BaseTest;
import listeners.TestListener;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.Accessories;
import pages.CheckOut;
import pages.HomePage;

@Listeners({TestListener.class})
public class ToolsQATest extends BaseTest {


    @Test
    public void placeOrderForMagicMouse() {
        Accessories accessories = pageGenerator.GetInstance(HomePage.class).goToAccesoriesPage();//go to accessories page

        Assert.assertEquals(accessories.getPageTitle(), "Accessories", "Not able to navigate to accessories page ");//verify landed

        Assert.assertTrue(accessories.verifyProductTitle("Magic Mouse"), "Magic mouse product not present on page");

        accessories.addToCartProduct("Magic Mouse");//add magic mouse to cart

        Assert.assertTrue(accessories.verifyElementPresence(accessories.addToCartSuccessMsg), "Added to cart msg doesn't appear");//verify success msg

        accessories.waitTillElementHasText(By.cssSelector("#header_cart .count"), "1");

        int itemCount = accessories.getItemCountInCart();//get item count in cart

        Assert.assertTrue(itemCount == 1, "Item count not increased after adding product to cart. Actual item count is " + itemCount);//verify item increased

        CheckOut checkOut = accessories.checkOutProduct();//checkout added magic mouse

        Assert.assertEquals(checkOut.getPageTitle(), "Checkout", "Checkout page is not opened");//verify landed

        checkOut.fillCheckoutDetails();
    }
}
