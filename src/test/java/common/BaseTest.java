package common;

import common.drivers.DriverFactory;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    public WebDriver driver;
    public PageGenerator pageGenerator;

    @BeforeMethod(alwaysRun = true)
    @Parameters(value = {"browser"})
    public void beforeInvocation(@Optional("chrome") String browser) {
        System.out.println("In before Invocation");
        System.out.println("Test Method BeforeInvocation is started. " + Thread.currentThread().getId());
        DriverFactory.setDriver(browser);
        driver = DriverFactory.getDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);//implicit wait
        driver.manage().window().setSize(new Dimension(1024, 768));
        driver.get("http://store.demoqa.com/");
        //Instantiate the Page Class
        pageGenerator = new PageGenerator(driver);
    }

    @AfterMethod(alwaysRun = true)
    public void afterInvocation() {
        System.out.println("Test Method AfterInvocation is started. " + Thread.currentThread().getId());
        WebDriver driver = DriverFactory.getDriver();
        if (driver != null) {
            driver.quit();
        }
    }

    @AfterSuite
    public void afterSuite() {
        WebDriver driver = DriverFactory.getDriver();
        if (driver != null) {
            driver.quit();
        }
    }

}
