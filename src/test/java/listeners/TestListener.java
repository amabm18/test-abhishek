package listeners;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import common.BaseTest;
import common.drivers.DriverFactory;
import common.extentreports.ExtentManager;
import common.extentreports.ExtentTestManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TestListener extends BaseTest implements ITestListener {

    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    //Before starting all tests, below method runs.
    @Override
    public void onStart(ITestContext iTestContext) {
        System.out.println("Starting Test Suite " + iTestContext.getName());
        iTestContext.setAttribute("WebDriver", DriverFactory.getDriver());
    }

    //After ending all tests, below method runs.
    @Override
    public void onFinish(ITestContext iTestContext) {
        System.out.println("Finishing Test Suite " + iTestContext.getName());
        //Do tier down operations for extentreports reporting!
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        WebDriver driver = DriverFactory.getDriver();
        if (driver != null) {
            driver.quit();
        }
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println("Test method " + getTestMethodName(iTestResult) + " starts");
        //Start operation for extentreports.
        ExtentTestManager.startTest(iTestResult.getMethod().getMethodName(), "");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        ExtentTestManager.getTest().log(LogStatus.INFO, "Test method " + getTestMethodName(iTestResult) + " passed");
        //Extentreports log operation for passed tests.
        ExtentTestManager.getTest().log(LogStatus.PASS, "Test passed");
    }

    private static String getScreenshot(WebDriver driver, String screenshotName) {
        String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        //after execution, you could see a folder "FailedTestsScreenshots" under src folder
        String fileName = screenshotName + dateName + ".png";
        String destination = "ExtentReports" + File.separator + screenshotName + dateName + ".png";
        File finalDestination = new File(destination);

        try {
            FileUtils.copyFile(source, finalDestination);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Returns the captured file path
        return fileName;
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        logFailure(iTestResult);
    }

    private void logFailure(ITestResult iTestResult) {
        WebDriver webDriver = DriverFactory.getDriver();

        // Since an error occurred, take a screenshot so we have a record of what went wrong.
        String fileName = getScreenshot(webDriver, "testFailedScreenshot");

        // Log information on the test that failed, the stack trace from the Throwable that was thrown, and the
        // current URL that we are on to the ExtentTest's log.
        ExtentTest extentTest = ExtentTestManager.getTest();
        extentTest.log(LogStatus.FAIL, "The test case " + getTestMethodName(iTestResult) + " has failed!");
        Throwable throwable = iTestResult.getThrowable();
        extentTest.log(LogStatus.FAIL, "Stacktrace : ", throwable);
        extentTest.log(LogStatus.FAIL,
                "ScreenShot " + extentTest.addScreenCapture(fileName));
        extentTest.log(LogStatus.FAIL,
                "URL : " + webDriver.getCurrentUrl());
    }


    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        System.out.println("I am in onTestSkipped method " + getTestMethodName(iTestResult) + " skipped");
        //Extentreports log operation for skipped tests.
        ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        System.out.println("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
    }

}