package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckOut extends BasePage {
    public CheckOut(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".entry-title")
    public WebElement pageTitle;

    @FindBy(css = "[data-wpsc-meta-key=billingfirstname]")
    public WebElement firstName;

    @FindBy(css = "[data-wpsc-meta-key=billinglastname]")
    public WebElement lastName;

    @FindBy(css = "[data-wpsc-meta-key=billingaddress]")
    public WebElement address;

    @FindBy(css = "[data-wpsc-meta-key=billingcity]")
    public WebElement city;

    @FindBy(css = "[data-wpsc-meta-key=billingstate]")
    public WebElement state;

    @FindBy(css = "[data-wpsc-meta-key=billingcountry]")
    public WebElement country;

    @FindBy(css = "[data-wpsc-meta-key=billingemail]")
    public WebElement email;

    @FindBy(css = "[data-wpsc-meta-key=billingphone]")
    public WebElement phone;

    @FindBy(css = "[data-wpsc-meta-key=billingpostcode]")
    public WebElement postalCode;

    @FindBy(css = "[data-wpsc-meta-key=billingregion]")
    public WebElement region;

    @FindBy(css = "[data-wpsc-meta-key=shippingSameBilling]")
    public WebElement sameAsBilling;

    @FindBy(css = "[data-wpsc-meta-key=shippingfirstname]")
    public WebElement shippingFirstName;

    @FindBy(css = "[data-wpsc-meta-key=shippinglastname]")
    public WebElement shippingLastName;

    @FindBy(css = "[data-wpsc-meta-key=shippingaddress]")
    public WebElement shippingAddress;

    @FindBy(css = "[data-wpsc-meta-key=shippingcity]")
    public WebElement shippingCity;

    @FindBy(css = "[data-wpsc-meta-key=shippingstate]")
    public WebElement shippingState;

    @FindBy(css = "[data-wpsc-meta-key=shippingcountry]")
    public WebElement shippingCountry;

    @FindBy(css = "[data-wpsc-meta-key=shippingpostcode]")
    public WebElement shippingPostalCode;

    @FindBy(css = "[data-wpsc-meta-key=shippingregion]")
    public WebElement shippingRegion;

    public String getPageTitle() {
        return pageTitle.getText();
    }

    public void fillCheckoutDetails() {
        type(firstName, "testUser");
        //the page isn't visible
    }
}
