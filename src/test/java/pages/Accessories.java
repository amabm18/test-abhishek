package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Accessories extends BasePage {
    public Accessories(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".entry-title")
    public WebElement pageTitle;

    @FindBy(css = ".wpsc_product_title")
    public List<WebElement> productTitle;

    @FindBy(xpath = "//form[contains(@action,'magic-mouse')] //input[@name='Buy']")
    public WebElement magicMouseAddToCart;

    @FindBy(css = "[title='Grid View']")
    public WebElement gridView;

    @FindBy(css = "[title='Default View']")
    public WebElement listView;

    @FindBy(css = ".alert.addtocart")
    public WebElement addToCartSuccessMsg;

    @FindBy(css = "#header_cart .cart_icon")
    public WebElement checkOut;

    @FindBy(css = "#header_cart .count")
    public WebElement itemCount;


    public boolean verifyProductTitle(String title) {
        List<WebElement> webElementList = productTitle;
        for (WebElement element : webElementList) {
            if (element.getText().equalsIgnoreCase(title)) {
                return true;
            }
        }
        return false;
    }

    public WebElement getAddToCartButton(String itemName) {
        String elemToSearch = itemName.replaceAll(" ", "-").toLowerCase().trim();
        return driver.findElement(By.xpath("//form[contains(@action,'" + elemToSearch + "')] //input[@name='Buy']"));
    }

    public String getPageTitle() {
        return getText(pageTitle);
    }

    public void addToCartProduct(String product) {
        click(getAddToCartButton(product));
    }

    public int getItemCountInCart() {
        return Integer.parseInt(getText(itemCount).replaceAll("\"", "").trim());
    }

    public CheckOut checkOutProduct() {
        click(checkOut);
        return PageFactory.initElements(driver, CheckOut.class);
    }

}
