package pages;

import common.PageGenerator;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage extends PageGenerator {

    public BasePage(WebDriver driver) {
        super(driver);
    }

    //If we need we can use custom wait in BasePage and all page classes
    WebDriverWait wait = new WebDriverWait(this.driver, 30);

    //Click Method by using JAVA Generics (You can use both By or Webelement)
    public <T> void click(T elementAttr) {
        if (elementAttr.getClass().getName().contains("By")) {
            driver.findElement((By) elementAttr).click();
        } else {
            ((WebElement) elementAttr).click();
        }
    }

    public <T> void mouseOver(T elementAttr) {
        Actions actions = new Actions(this.driver);
        if (elementAttr.getClass().getName().contains("By")) {
            actions.moveToElement(driver.findElement((By) elementAttr)).build().perform();
        } else {
            actions.moveToElement((WebElement) elementAttr).build().perform();
        }
    }


    //Write Text by using JAVA Generics (You can use both By or Webelement)
    public <T> void type(T elementAttr, String text) {
        if (elementAttr.getClass().getName().contains("By")) {
            driver.findElement((By) elementAttr).sendKeys(text);
        } else {
            ((WebElement) elementAttr).sendKeys(text);
        }
    }

    //Read Text by using JAVA Generics (You can use both By or Webelement)
    public <T> String getText(T elementAttr) {
        if (elementAttr.getClass().getName().contains("By")) {
            return driver.findElement((By) elementAttr).getText();
        } else {
            return ((WebElement) elementAttr).getText();
        }
    }


    public boolean waitTillElementHasText(By elementAttr, String text) {
        if (elementAttr.getClass().getName().contains("By")) {
            return wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(elementAttr), text));
        } else {
            return wait.until(ExpectedConditions.textToBePresentInElement((WebElement) elementAttr, text));
        }
    }


    public <T> boolean verifyElementPresence(T elementAttr) {
        try {
            if (elementAttr.getClass().getName().contains("By")) {
                driver.findElement((By) elementAttr);
                return true;
            } else {
                WebElement element = ((WebElement) elementAttr);
                return true;
            }

        } catch (NoSuchElementException e) {
            return false;
        }
    }


}