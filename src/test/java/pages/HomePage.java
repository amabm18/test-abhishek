package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#logo")
    public WebElement logoToolsQA;

    @FindBy(css = ".menu-item-home>a")
    public WebElement homeMenuTab;

    @FindBy(css = ".menu-item-object-wpsc_product_category.menu-item-has-children>a")
    public WebElement productCatagoryTab;

    public WebElement getSubMenuUnderProductCategory(String subMenuName) {
        return driver.findElement(By.xpath("//*[contains(@class,'menu-item')] //a[contains(@href,'" + subMenuName + "')]"));
    }

    public Accessories goToAccesoriesPage() {
        mouseOver(productCatagoryTab);
        click(getSubMenuUnderProductCategory("accessories"));
        return PageFactory.initElements(driver, Accessories.class);
    }


}
