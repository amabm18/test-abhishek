package common.drivers;

/**
 * The operating system that is currently being used.
 */
public enum OS {

    LINUX_64,
    MAC,
    WINDOWS,
    UNKNOWN;

    public static OS getOS() {
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Windows")) {
            return WINDOWS;
        } else if (osName.startsWith("Mac OS X") || osName.startsWith("Darwin")) {
            return MAC;
        } else {
            String sunArchDataModel = System.getProperty("sun.arch.data.model");
            if (sunArchDataModel.equals("64")) {
                return LINUX_64;
            } else {
                return UNKNOWN;
            }
        }
    }
}
