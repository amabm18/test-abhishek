package common.utils;

import common.downloader.ChromeDriverDownloader;
import common.downloader.GeckoDriverDownloader;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;

public class OptionsUtils {

    //Get Chrome Options
    public ChromeOptions getChromeOptions() {
        ChromeDriverDownloader chromeDriverDownloader = new ChromeDriverDownloader();
        File chromeDriverLocation = chromeDriverDownloader.downloadAndExtract();
        System.setProperty("webdriver.chrome.driver", chromeDriverLocation.getAbsolutePath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-popup-blocking");
        options.addArguments("--incognito");
        return options;
    }

    //Get Firefox Options
    public FirefoxOptions getFirefoxOptions() {
        GeckoDriverDownloader geckoDriverDownloader = new GeckoDriverDownloader();
        File geckoDriverLocation = geckoDriverDownloader.downloadAndExtract();
        System.setProperty("webdriver.gecko.driver", geckoDriverLocation.getAbsolutePath());
        FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile profile = new FirefoxProfile();
        //Accept Untrusted Certificates
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(false);
        //Use No Proxy Settings
        profile.setPreference("network.proxy.type", 0);
        //Set Firefox profile to capabilities
        options.setCapability(FirefoxDriver.PROFILE, profile);
        return options;
    }
}
