package common.utils;

import com.google.common.io.ByteSink;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import common.drivers.OS;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * A utility class containing methods to extract WebDrivers from objects.
 */
public class DriverUtils {

    public static void extractExe(String url, File installDir, File file, OS operatingSystem, String driveName) {

        // Do nothing if the file's already been extracted.
        if (file.exists()) {
            return;
        }

        File targetZip = new File(installDir, driveName + ".zip");
        downloadZip(url, targetZip);
        try {
            if (operatingSystem == OS.WINDOWS) {
                unzip(targetZip, installDir);
            } else if (operatingSystem == OS.MAC && driveName.equalsIgnoreCase("chromedriver")) {
                ProcessBuilder processBuilder = new ProcessBuilder();
                ProcessBuilder command = processBuilder.command("/usr/bin/unzip", "-qo", driveName + ".zip");
                ProcessBuilder directory = command.directory(installDir);
                Process start = directory.start();
                start.waitFor();
            } else {
                ProcessBuilder processBuilder = new ProcessBuilder();
                ProcessBuilder tar = processBuilder.command("tar", "-xjvf", driveName + ".zip");
                ProcessBuilder directory = tar.directory(installDir);
                Process start = directory.start();
                start.waitFor();
            }
        } catch (Exception e) {
            throw new IllegalStateException("Unable to unzip driver from " + targetZip.getAbsolutePath());
        }
    }

    private static void downloadZip(String url, File targetZip) {

        // Do nothing if the file already exists.
        if (targetZip.exists()) {
            return;
        }


        File zipTemp = new File(targetZip.getAbsolutePath() + ".temp");
        try {
            File parentFile = zipTemp.getParentFile();
            parentFile.mkdirs();

            URI uri = URI.create(url);
            ByteSource input = Resources.asByteSource(uri.toURL());
            ByteSink output = Files.asByteSink(zipTemp);

            input.copyTo(output);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to download driver from " + url);
        }

        zipTemp.renameTo(targetZip);
    }

    private static void unzip(File zip, File toDir) throws IOException {
        try (ZipFile zipFile = new ZipFile(zip)) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                final ZipEntry entry = entries.nextElement();
                if (entry.isDirectory()) {
                    continue;
                }

                File to = new File(toDir, entry.getName());
                File parentFile = to.getParentFile();
                parentFile.mkdirs();

                ByteSource byteSource = new ByteSource() {
                    @Override
                    public InputStream openStream() throws IOException {
                        return zipFile.getInputStream(entry);
                    }
                };
                byteSource.copyTo(Files.asByteSink(to));
            }
        }
    }

}
