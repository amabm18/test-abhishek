package common.downloader;

import common.drivers.OS;

/**
 * A downloader for a WebDriver.
 */
class DriverDownloader {
    OS operatingSystem;

    DriverDownloader() {
        operatingSystem = OS.getOS();
    }

}
