package common.downloader;

import common.utils.DriverUtils;

import java.io.File;

/**
 * A dynamic downloader for a Chrome Driver.
 */
@SuppressWarnings("UnstableApiUsage")
public
class ChromeDriverDownloader extends DriverDownloader {

    /**
     * Depending on what this operating system is, download and extract the appropriate Chrome Driver and return it as a
     * File.
     *
     * @return The File representation of the ChromeDriver.
     */
    public File downloadAndExtract() {
        File installDir = new File(new File(System.getProperty("user.home")), ".chromedriver");
        String url;
        File chromeDriver;
        switch (operatingSystem) {
            case WINDOWS:
                url = "https://chromedriver.storage.googleapis.com/2.43/chromedriver_win32.zip";
                chromeDriver = new File(installDir, "chromedriver.exe");
                break;
            case MAC:
                url = "https://chromedriver.storage.googleapis.com/2.43/chromedriver_mac64.zip";
                chromeDriver = new File(installDir, "chromedriver");
                break;
            case LINUX_64:
                url = "https://chromedriver.storage.googleapis.com/2.43/chromedriver_linux64.zip";
                chromeDriver = new File(installDir, "chromedriver");
                break;
            default:
                url = "https://chromedriver.storage.googleapis.com/2.43/chromedriver_linux32.zip";
                chromeDriver = new File(installDir, "chromedriver");
                break;
        }

        DriverUtils.extractExe(url, installDir, chromeDriver, operatingSystem, "chromedriver");
        return chromeDriver;
    }


}