package common.downloader;

import common.utils.DriverUtils;

import java.io.File;

/**
 * A downloader for the Gecko WebDriver for Firefox.
 */
public class GeckoDriverDownloader extends DriverDownloader {

    /**
     * Download and extract the Firefox WebDriver, and return it as a File object.
     */
    public File downloadAndExtract() {
        File installDir = new File(new File(System.getProperty("user.home")), ".geckodriver");
        String url;
        File geckoDriver;


        final String versionNumber = "0.23.0";
        final String baseLink =
                String.format("https://github.com/mozilla/geckodriver/releases/download/v%s", versionNumber);
        switch (operatingSystem) {

            case WINDOWS:
                url = String.format("%s/geckodriver-v%s-win64.zip", baseLink, versionNumber);
                geckoDriver = new File(installDir, "geckodriver.exe");
                break;
            case MAC:
                url =
                        String.format("%s/geckodriver-v%s-macos.tar.gz", baseLink, versionNumber);
                geckoDriver = new File(installDir, "geckodriver");
                break;
            case LINUX_64:
                url =
                        String.format("%s/geckodriver-v%s-linux64.tar.gz", baseLink, versionNumber);
                geckoDriver = new File(installDir, "geckodriver");
                break;
            default:
                url =
                        String.format("%s/geckodriver-v%s-linux32.tar.gz", baseLink, versionNumber);
                geckoDriver = new File(installDir, "geckodriver");
                break;
        }

        DriverUtils.extractExe(url, installDir, geckoDriver, operatingSystem, "geckodriver");
        return geckoDriver;
    }
}
